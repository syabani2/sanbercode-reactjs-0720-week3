import React, { useState, useEffect } from "react";
import axios from "axios"


const Lists = () => {

    const [dataHargaBuah, setDataHargaBuah] = useState(null);
    const [inputBuah, setInputBuah] = useState(null);
    const [inputHarga, setInputHarga] = useState(null);
    const [inputBerat, setInputBerat] = useState(null);
    const [selectedId, setSelectedId] = useState(0)
    const [statusForm, setStatusForm] = useState("create")

    useEffect(() => {
        if (dataHargaBuah === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
                .then(res => {
                    setDataHargaBuah(res.data.map(el => { return { id: el.id, name: el.name, price: el.price, weight: el.weight } }))
                })
        }
    }, [dataHargaBuah])

    const handleDelete = (event) => {
        let idBuah = parseInt(event.target.value)

        let newDataHargaBuah = dataHargaBuah.filter(el => el.id !== idBuah)

        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
            .then(res => {
                console.log(res)
            })

        setDataHargaBuah([...newDataHargaBuah])
    }

    const handleEdit = (event) => {
        let idBuah = parseInt(event.target.value)
        let buah = dataHargaBuah.find(x => x.id === idBuah)
        setInputBuah(buah.name)
        setInputHarga(buah.price)
        setInputBerat(buah.weight)
        setSelectedId(idBuah)
        setStatusForm("edit")
    }

    const handleChangeBuah = (event) => {
        setInputBuah(event.target.value);
    }

    const handleChangeHarga = (event) => {
        setInputHarga(event.target.value);
    }

    const handleChangeBerat = (event) => {
        setInputBerat(event.target.value);
    }

    const handleSubmit = (event) => {
        // menahan submit
        event.preventDefault()

        let buah = inputBuah
        let harga = inputHarga
        let berat = inputBerat
        console.log(buah, harga, berat)

        if (buah.replace(/\s/g, '') !== "") {
            if (statusForm === "create") {
                axios.post(`http://backendexample.sanbercloud.com/api/fruits`, { name:buah, price:harga, weight:berat })
                    .then(res => {
                        setDataHargaBuah([...dataHargaBuah, {id: res.data.id, name: buah, price: harga, weight: berat }])
                    })
            } else if (statusForm === "edit") {
                axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectedId}`, { name:buah, price:harga, weight:berat })
                    .then(res => {
                        let dataBuah = dataHargaBuah.find(el => el.id === selectedId)
                        dataBuah.name = buah
                        dataBuah.price = harga
                        dataBuah.weight = berat
                        setDataHargaBuah([...dataHargaBuah])
                    })
            }

            setStatusForm("create")
            setSelectedId(0)
            setInputBuah("")
            setInputBerat("")
            setInputHarga("")
        }
    }

    return (
        <>
            <h1>Tabel Harga Buah</h1>
            <table className="content">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th style={{ width: '100px' }}>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        dataHargaBuah !== null && dataHargaBuah.map((val, index) => {
                            console.log(val)
                            return (
                                <tr key={index}>
                                    <td>{val.name}</td>
                                    <td>{val.price}</td>
                                    <td>{val.weight / 1000} Kg </td>
                                    <td>
                                        <button onClick={handleEdit} value={val.id}>Edit</button>
                                        &nbsp;
                                        <button onClick={handleDelete} value={val.id}>Delete</button>
                                    </td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
            {/* Form */}
            <h1>Form Daftar Buah</h1>
            <form onSubmit={handleSubmit}>
                <table>
                    <tbody>
                        <tr>
                            <td>Masukan Nama Buah</td>
                            <td>: <input type="text" value={inputBuah} onChange={handleChangeBuah}></input></td>
                        </tr>
                        <tr>
                            <td>Masukan Harga Buah</td>
                            <td>: <input type="number" value={inputHarga} onChange={handleChangeHarga}></input></td>
                        </tr>
                        <tr>
                            <td>Masukan Berat Buah</td>
                            <td>: <input type="number" value={inputBerat} onChange={handleChangeBerat}></input></td>
                        </tr>
                    </tbody>
                </table>
                <button style={{ marginTop: "15px" }}>submit</button>
            </form>
        </>
    );
}

export default Lists;