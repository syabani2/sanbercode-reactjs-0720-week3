import React from 'react';
import {NavProvider} from './NavigationContext.js';
import NavigationList from './NavigationList.js';

const Navigation = () => {
    return(
        <NavProvider>
            <NavigationList></NavigationList>
        </NavProvider>
    )
}

export default Navigation;