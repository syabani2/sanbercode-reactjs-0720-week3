import React, {useContext} from 'react';
import axios from "axios";
import {BuahContext} from './BuahContext.js';

const BuahForm = () => {
    const {buahContext, namaContext, hargaContext, beratContext, idContext, formContext} = useContext(BuahContext);
    const [dataBuah, setDataBuah] = buahContext;
    const [inputBuah, setInputBuah] = namaContext;
    const [inputHarga, setInputHarga] = hargaContext;
    const [inputBerat, setInputBerat] = beratContext;
    const [selectedId, setSelectedId] = idContext;
    const [statusForm, setStatusForm] = formContext;

    const handleSubmit = (event) => {
        event.preventDefault()

        if (inputBuah.replace(/\s/g, '') !== "") {
            if (statusForm === "create") {
                axios.post(`http://backendexample.sanbercloud.com/api/fruits`, { name: inputBuah, price: inputHarga, weight: inputBerat })
                    .then(res => {
                        setDataBuah([...dataBuah, {id: res.data.id, name: inputBuah, price: inputHarga, weight: inputBerat }])
                    })
            } else if (statusForm === "edit") {
                axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectedId}`, { name: inputBuah, price: inputHarga, weight: inputBerat })
                    .then(res => {
                        let dataBuahTemp = dataBuah.find(el => el.id === selectedId)
                        dataBuahTemp.name = inputBuah
                        dataBuahTemp.price = inputHarga
                        dataBuahTemp.weight = inputBerat
                        setDataBuah([...dataBuah])
                    })
            }

            setStatusForm("create")
            setSelectedId(0)
            setInputBuah("")
            setInputBerat("")
            setInputHarga("")
        }
    }

    const handleChangeBuah = (event) => {
        setInputBuah(event.target.value);
    }

    const handleChangeHarga = (event) => {
        setInputHarga(event.target.value);
    }

    const handleChangeBerat = (event) => {
        setInputBerat(event.target.value);
    }

    return (
        <>
            <h1>Form Daftar Buah</h1>
            <form onSubmit={handleSubmit}>
                <table>
                    <tbody>
                        <tr>
                            <td>Masukan Nama Buah</td>
                            <td>: <input type="text" value={inputBuah} onChange={handleChangeBuah}></input></td>
                        </tr>
                        <tr>
                            <td>Masukan Harga Buah</td>
                            <td>: <input type="number" value={inputHarga} onChange={handleChangeHarga}></input></td>
                        </tr>
                        <tr>
                            <td>Masukan Berat Buah</td>
                            <td>: <input type="number" value={inputBerat} onChange={handleChangeBerat}></input></td>
                        </tr>
                    </tbody>
                </table>
                <button style={{ marginTop: "15px" }}>submit</button>
            </form>
        </>
    )
}

export default BuahForm;
