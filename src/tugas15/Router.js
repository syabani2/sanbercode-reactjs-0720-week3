import React from "react";
import { Switch, Route } from "react-router-dom";

import UserInfo from '../tugas11/HargaBuah';
import Timer from '../tugas12/Timer';
import Lists from '../tugas13/Lists';
import HooksLists from '../tugas14/Lists';
import Buah from '../tugas15/Buah';
import Navigation from './Navigation';

const Routes = () => {
    return (
        <>
            <Navigation/>
            <Switch>
                <Route path="/timer">
                    <Timer time={100}></Timer>
                </Route>
                <Route path="/lists">
                    <Lists />
                </Route>
                <Route path="/hook-lists">
                    <HooksLists />
                </Route>
                <Route path="/buah">
                    <Buah />
                </Route>
                <Route path="/">
                    <UserInfo />
                </Route>
            </Switch>
        </>
    );
};

export default Routes;
