import React, { useState, createContext, useEffect } from 'react';
import axios from "axios";
export const BuahContext = createContext()

export const BuahProvider = props => {
    const [dataBuah, setDataBuah] = useState(null);
    const [inputBuah, setInputBuah] = useState(null);
    const [inputHarga, setInputHarga] = useState(null);
    const [inputBerat, setInputBerat] = useState(null);
    const [selectedId, setSelectedId] = useState(0)
    const [statusForm, setStatusForm] = useState("create")

    useEffect(() => {
        if (dataBuah === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
                .then(res => {
                    setDataBuah(res.data.map(el => { return { id: el.id, name: el.name, price: el.price, weight: el.weight } }))
                })
        }
    }, [dataBuah])


    return (
        <BuahContext.Provider value={{
            buahContext: [dataBuah, setDataBuah],
            namaContext: [inputBuah, setInputBuah],
            hargaContext: [inputHarga, setInputHarga],
            beratContext: [inputBerat, setInputBerat],
            idContext: [selectedId, setSelectedId],
            formContext: [statusForm, setStatusForm]
        }}>
            {props.children}
        </BuahContext.Provider>
    )
}
