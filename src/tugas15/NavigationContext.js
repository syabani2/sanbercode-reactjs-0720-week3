import React, { useState, createContext} from "react";

export const NavigationContext = createContext();

export const NavProvider = props => {
    const [changeColorTheme, setChangeColorTheme] = useState(false);

    return (
    <NavigationContext.Provider value={[changeColorTheme, setChangeColorTheme]}>
        {props.children}
    </NavigationContext.Provider>);
};