import React from 'react';
import {BuahProvider} from './BuahContext.js';
import BuahList from './BuahList.js';
import BuahForm from './BuahForm.js';

const Buah = () => {
    return(
        <BuahProvider>
            <BuahList></BuahList>
            <BuahForm></BuahForm>
        </BuahProvider>
    )
}

export default Buah;