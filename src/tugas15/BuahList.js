import React, {useContext} from "react";
import axios from "axios";
import {BuahContext} from './BuahContext';

const BuahList = () => {
    const {buahContext, namaContext, hargaContext, beratContext, idContext, formContext} = useContext(BuahContext);
    const [dataBuah, setDataBuah] = buahContext;
    const [inputBuah, setInputBuah] = namaContext;
    const [inputHarga, setInputHarga] = hargaContext;
    const [inputBerat, setInputBerat] = beratContext;
    const [selectedId, setSelectedId] = idContext;
    const [statusForm, setStatusForm] = formContext;

    const handleEdit = (event) => {
        let idBuah = parseInt(event.target.value)
        let buah = dataBuah.find(x => x.id === idBuah)
        setSelectedId(idBuah)
        setInputBuah(buah.name === null ? "" : buah.name)
        setInputHarga(buah.price === null ? 0 : buah.price)
        setInputBerat(buah.weight === null ? 0 : buah.weight)
        setStatusForm("edit")
    }

    const handleDelete = (event) => {
        let idBuah = parseInt(event.target.value)

        let newDataBuah = dataBuah.filter(el => el.id !== idBuah)

        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
            .then(res => {
                console.log(res)
            })

        setDataBuah([...newDataBuah])
    }

    return (
        <>
            <h1>Tabel Harga Buah</h1>
            <table className="content">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th style={{ width: '100px' }}>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        dataBuah !== null && dataBuah.map((val, index) => {
                            console.log(val)
                            return (
                                <tr key={index}>
                                    <td>{val.name}</td>
                                    <td>{val.price}</td>
                                    <td>{val.weight / 1000} Kg </td>
                                    <td>
                                        <button onClick={handleEdit} value={val.id}>Edit</button>
                                        &nbsp;
                                        <button onClick={handleDelete} value={val.id}>Delete</button>
                                    </td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
        </>
    )
}

export default BuahList;