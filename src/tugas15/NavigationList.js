import React, { useState}  from "react"
import {Link} from 'react-router-dom';
import {NavigationContext} from "./NavigationContext"

const NavigationList = () =>{
    const [changeColorTheme,setChangeColorTheme] = useState(NavigationContext);

    return(
        <>
            <nav>
                <ul className={`${changeColorTheme ? "ocean":"normal"}`}>
                    <li id="home">
                        <Link to="/">Tugas 11</Link>
                    </li>
                    <li id="timer">
                        <Link to="/timer">Tugas 12</Link>
                    </li>
                    <li id="lists">
                        <Link to="/lists">Tugas 13</Link>
                    </li>
                    <li id="hook-lists">
                        <Link to="/hook-lists">Tugas 14</Link>
                    </li>
                    <li id="buah">
                        <Link to="/buah">Tugas 15</Link>
                    </li>
                    <li>
                        <button className="button-theme" onClick={()=>setChangeColorTheme(!changeColorTheme)} style={{marginLeft: '15px', marginTop: '10px'}}> Change Theme </button>
                    </li>
                </ul>
            </nav>
        </>
    )
}

export default NavigationList