import React, { Component } from 'react'

class Timer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            time: this.props.time,
            hour: new Date().toLocaleTimeString()
        }
    }

    componentDidMount() {
        if (this.props.start !== undefined) {
            this.setState({ time: this.props.start })
        }
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentDidUpdate() {
        if(this.state.time < 0) {
            this.componentWillUnmount()
        }
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            time: this.state.time - 1,
            hour: new Date().toLocaleTimeString()
        });
    }


    render() {
        return (
            <>
                {this.state.time >= 0 &&
                    <table className="timer">
                        <td className="hour">
                            Sekarang Jam: {this.state.hour}
                        </td>
                        <td className="countdown">
                            Hitung Mundur: {this.state.time}
                        </td>
                    </table>
                }
            </>
        )
    }
}

export default Timer