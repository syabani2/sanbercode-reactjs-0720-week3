import React from "react";

class FruitName extends React.Component {
    render() {
        return <td>{this.props.nama}</td>;
    }
}

class ShowPrice extends React.Component {
    render() {
        return <td>{this.props.harga}</td>
    }
}

class ShowWeight extends React.Component {
    render() {
        return <td>{this.props.berat / 1000} Kg</td>
    }
}


let dataHargaBuah = [
    { nama: "Semangka", harga: 10000, berat: 1000 },
    { nama: "Anggur", harga: 40000, berat: 500 },
    { nama: "Strawberry", harga: 30000, berat: 400 },
    { nama: "Jeruk", harga: 30000, berat: 1000 },
    { nama: "Mangga", harga: 30000, berat: 500 },
];

class FruitPrice extends React.Component {
    render() {
        return (
            <>
                <h1>Tabel Harga Buah</h1>
                <table className="content">
                    <tr>
                        <th>
                            Nama
                        </th>
                        <th>
                            Harga
                        </th>
                        <th>
                            Berat
                        </th>
                    </tr>
                    {
                        dataHargaBuah.map((fruitData) => {
                            return (
                                <tr>
                                    <FruitName nama={fruitData.nama} />
                                    <ShowPrice harga={fruitData.harga} />
                                    <ShowWeight berat={fruitData.berat} />
                                </tr>
                            );
                        })
                    }
                </table>
            </>
        );
    }
}

export default FruitPrice;