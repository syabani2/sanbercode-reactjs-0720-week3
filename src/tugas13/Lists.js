import React, { Component } from "react";

class Lists extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataHargaBuah: [
                { nama: "Semangka", harga: 10000, berat: 1000 },
                { nama: "Anggur", harga: 40000, berat: 500 },
                { nama: "Strawberry", harga: 30000, berat: 400 },
                { nama: "Jeruk", harga: 30000, berat: 1000 },
                { nama: "Mangga", harga: 30000, berat: 500 },
            ],
            inputBuah: "",
            inputHarga: "",
            inputBerat: "",
            indexOfForm: -1
        }

        this.handleChangeBuah = this.handleChangeBuah.bind(this);
        this.handleChangeHarga = this.handleChangeHarga.bind(this);
        this.handleChangeBerat = this.handleChangeBerat.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleChangeBuah(event) {
        this.setState({ inputBuah: event.target.value });
    }

    handleChangeHarga(event) {
        this.setState({ inputHarga: event.target.value });
    }

    handleChangeBerat(event) {
        this.setState({ inputBerat: event.target.value });
    }


    handleSubmit(event) {
        event.preventDefault()

        let name = this.state.inputBuah
        let harga = parseInt(this.state.inputHarga)
        let berat = parseInt(this.state.inputBerat)

        if (name.replace(/\s/g, '') !== "") {
            let newDataHargaBuah = this.state.dataHargaBuah
            let index = this.state.indexOfForm

            if (index === -1) {
                newDataHargaBuah = [...newDataHargaBuah, {nama: name, harga: harga, berat:berat}]
                console.log(newDataHargaBuah)
            } else {
                newDataHargaBuah[index] = {nama: name, harga: harga, berat:berat}
            }

            this.setState({
                dataHargaBuah: newDataHargaBuah,
                inputBuah: "",
                inputHarga: "",
                inputBerat: ""
            })
        }

    }

    handleDelete(event) {
        let index = event.target.value;
        let newDataHargaBuah = this.state.dataHargaBuah;
        let editedDataHargaBuah = newDataHargaBuah[this.state.indexOfForm]
        newDataHargaBuah.splice(index, 1)

        if (editedDataHargaBuah !== undefined) {
            var newIndex = newDataHargaBuah.findIndex((el) => el === editedDataHargaBuah)
            this.setState({ dataHargaBuah: newDataHargaBuah, indexOfForm: newIndex })
        } else {
            this.setState({ dataHargaBuah: newDataHargaBuah })
        }
    }

    handleEdit(event) {
        let index = event.target.value
        let nama = this.state.dataHargaBuah[index]['nama']
        let harga = this.state.dataHargaBuah[index]['harga']
        let berat = this.state.dataHargaBuah[index]['berat']
        this.setState({ inputBuah: nama, indexOfForm: index })
        this.setState({ inputHarga: harga, indexOfForm: index })
        this.setState({ inputBerat: berat, indexOfForm: index })
    }

    render() {
        return (
            <>
                <h1>Tabel Harga Buah</h1>
                <table className="content">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Berat</th>
                            <th style={{ width: '100px' }}>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.dataHargaBuah.map((val, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{val.nama}</td>
                                        <td>{val.harga}</td>
                                        <td>{val.berat / 1000} Kg </td>
                                        <td>
                                            <button onClick={this.handleEdit} value={index}>Edit</button>
                                            &nbsp;
                                            <button onClick={this.handleDelete} value={index}>Delete</button>
                                        </td>
                                    </tr>
                                );
                            })
                        }
                    </tbody>
                </table>
                {/* Form */}
                <h1>Form Daftar Buah</h1>
                <form onSubmit={this.handleSubmit}>
                    <table>
                        <tbody>
                            <tr>
                                <td>Masukan Nama Buah</td>
                                <td>: <input type="text" value={this.state.inputBuah} onChange={this.handleChangeBuah}></input></td>
                            </tr>
                            <tr>
                                <td>Masukan Harga Buah</td>
                                <td>: <input type="number" value={this.state.inputHarga} onChange={this.handleChangeHarga}></input></td>
                            </tr>
                            <tr>
                                <td>Masukan Berat Buah</td>
                                <td>: <input type="number" value={this.state.inputBerat} onChange={this.handleChangeBerat}></input></td>
                            </tr>
                        </tbody>
                    </table>
                    <button style={{ marginTop: "15px" }}>submit</button>
                </form>
            </>
        );
    }
}

export default Lists;