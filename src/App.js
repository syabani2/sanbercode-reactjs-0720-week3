import React from 'react';
import {BrowserRouter as Router} from "react-router-dom"
// import FruitPrice from './tugas11/HargaBuah.js';
// import Lists from './tugas13/Lists.js';
// import Lists from './tugas14/Lists.js';
// import Timer from './tugas12/Timer.js';
// import Buah from './tugas15/Buah.js';
import Routes from './tugas15/Router';
import './App.css';


function App() {
  return (
    <div className="App">
      <div className="container">
        {/* <FruitPrice /> */}
        {/* <Timer time={100}/> */}
        <Router>
          <Routes></Routes>
        </Router>
      </div>
    </div>
  );
}

export default App;
